package org.example;

import java.util.ArrayList;
import java.util.Scanner;

class Employee {
    int id;
    String name;
    String sex;
    String email;
    int phone_no;
    float salary;

    public Employee(int id, String name, String sex, String email, int phone_no, float salary)
    {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.email = email;
        this.phone_no = phone_no;
        this.salary = salary;
    }

    public String toString()
    {
        return "Employee Details: " + "\nID: " + this.id + "\nName: " + this.name + "\nSex: " + this.sex + "\nEmail: " + this.email + "\nPhone Number: " + this.phone_no + "\nSalary: " + this.salary;
    }
}
public class Main {

    static void display(ArrayList<Employee> employees)
    {
        System.out.println("Employee List ");
        System.out.printf("%-10s%-25s%-13s%-20s%-15s%-7s%n","ID","Name", "Sex", "Email", "Phone Number", "Salary");
        for(Employee e : employees)
        {
            System.out.printf("%-10s%-25s%-13s%-20s%-15s%-7.2f%n", e.id,e.name,e.sex,e.email,e.phone_no,e.salary);
        }
    }
    public static void main(String[] args) {
        int id;
        String name;
        String sex;
        String email;
        int phone_no;
        float salary;

        Scanner in = new Scanner(System.in);
        ArrayList<Employee> employees = new ArrayList<Employee>();
        String fname, lname, sexx;

        do
        {
            System.out.println("\nWELCOME TO THE EMPLOYEE MANAGEMENT SYSTEM\n");
            System.out.println("1). Add Employee to the DataBase\n" +
                    "2). Search for Employee\n" +
                    "3). Update Employee details\n" +
                    "4). Delete Employee Details\n" +
                    "5). Display all Employees working in this company\n" +
                    "6). EXIT\n");
            System.out.println("Enter your choice : ");
            int ch = in.nextInt();

            switch(ch)
            {
                case 1:System.out.println("\nEnter the following details:\n");
                    System.out.println("Enter ID:");
                    id = in.nextInt();
                    System.out.println("Enter First Name:");
                    fname = in.next();
                    System.out.println("Enter Last Name:");
                    lname = in.next();
                    name = fname + " " + lname;
                    System.out.println("Enter Sex(M/F):");
                    sexx = in.next();
                    sexx.toLowerCase();
                    if(sexx.equals("m"))
                        sex = "Male";
                    else if (sexx.equals("f")) {
                        sex = "Female";
                    }
                    else
                        sex = "Non-Binary";
                    System.out.println("Enter Email:");
                    email = in.next();
                    System.out.println("Enter Phone Number:");
                    phone_no = in.nextInt();
                    System.out.println("Enter Salary:");
                    salary = in.nextFloat();
                    employees.add(new Employee(id, name, sex, email, phone_no, salary));
                    break;

                case 2: System.out.println("Enter the Employee ID to search:");
                    id = in.nextInt();
                    int i=0;
                    for(Employee e: employees)
                    {
                        if(id == e.id)
                        {
                            System.out.println(e+"\n");
                            i++;
                        }
                    }
                    if(i == 0)
                    {
                        System.out.println("\nEmployee Details not found, Please enter a valid ID");
                    }
                    break;

                case 3: System.out.println("\nEnter the Employee ID to EDIT the details");
                    id = in.nextInt();
                    int j=0;
                    for(Employee e: employees)
                    {
                        if(id == e.id)
                        {
                            j++;
                            do{
                                int ch1 =0;
                                System.out.println("\nEDIT Employee Details:\n" +
                                        "1). Employee ID\n" +
                                        "2). Name\n" +
                                        "3). Sex\n" +
                                        "4). Email\n" +
                                        "5). Phone Number\n"+
                                        "6). Salary\n" +
                                        "7). GO BACK\n");
                                System.out.println("Enter your choice: ");
                                ch1 = in.nextInt();
                                switch(ch1)
                                {
                                    case 1: System.out.println("\nEnter new Employee ID:");
                                        e.id =in.nextInt();
                                        System.out.println(e+"\n");
                                        break;

                                    case 2:
                                        System.out.println("Enter new Employee First Name:");
                                        fname = in.next();
                                        System.out.println("Enter new Employee Last Name:");
                                        lname = in.next();
                                        e.name = fname +" "+ lname;
                                        System.out.println(e+"\n");
                                        break;

                                    case 3: System.out.println("Enter new Employee Sex:");
                                        e.sex =in.next();
                                        System.out.println(e+"\n");
                                        break;

                                    case 4: System.out.println("Enter new Employee Email:");
                                        e.email =in.next();
                                        System.out.println(e+"\n");
                                        break;

                                    case 5: System.out.println("Enter new Employee Phone Number:");
                                        e.phone_no =in.nextInt();
                                        System.out.println(e+"\n");
                                        break;

                                    case 6: System.out.println("Enter new Employee Salary:");
                                        e.salary =in.nextFloat();
                                        System.out.println(e+"\n");
                                        break;

                                    case 7: j++;
                                        break;

                                    default : System.out.println("\nEnter a correct choice from the List:");
                                        break;

                                }
                            }
                            while(j==1);
                        }
                    }
                    if(j == 0)
                    {
                        System.out.println("\nEmployee Details not found, Please enter a valid ID");
                    }

                    break;

                case 4: System.out.println("\nEnter Employee ID to DELETE from the Database:");
                    id = in.nextInt();
                    int k=0;
                    try{
                        for(Employee e: employees)
                        {
                            if(id == e.id)
                            {
                                employees.remove(e);
                                display(employees);
                                k++;
                            }
                        }
                        if(k == 0)
                        {
                            System.out.println("\nEmployee Details not found, Please enter a valid ID");
                        }
                    }
                    catch(Exception ex){
                        System.out.println(ex);
                    }
                    break;

                case 5:
                    display(employees);
                    break;

                case 6:
                    System.out.println("\nExiting...");
                    System.exit(0);
                    break;

                default : System.out.println("\nEnter a correct choice from the List:");
                    break;

            }
        }
        while(true);
    }
}